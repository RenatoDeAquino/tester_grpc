import tratamento as trt

def escolha_rota():
    final_rpc = trt.tratamento_rpc()
    print("Digite qual opcao voce gostaria de testar")
    
    for x in range(0, len(final_rpc)):
        print(f'{x+1}) {final_rpc[x]}')
    
    escolha = input("->")
    
    if int(escolha) > len(final_rpc) or int(escolha) <= 0:
        return print("Erro no valor digitado")
        
    return final_rpc[int(escolha) -1]


def escolha_proto():
    final_proto = trt.tratamento_proto()
    
    for x in range(0, len(final_proto)):
        print(f'{x+1}) {final_proto[x]}')
        
    escolha = input("->")
    
    if int(escolha) > len(final_proto) or int(escolha) <= 0:
        return print("Erro no valor digitado")
        
    return final_proto[int(escolha) -1]


def generate_rota():
    final_rota = escolha_rota()
    final_package = trt.tratamento_package()
    final_service = trt.tratamento_service()
    
    final_retorno = f'{final_package}.{final_service}/{final_rota}'
    
    return final_retorno

print(escolha_proto())