import datetime
import os
import json
import rota as ro

def generate_ghz():
    chamada = open('../chamada/chamada_saldo.json', 'r')
    
    ghz_chamada = json.load(chamada)
    ghz_proto = ro.escolha_proto()
    ghz_final = str(ghz_chamada).replace("\'", "\"")
    
    ip = input("Digite o ip\n->")
    porta = input("Digite a porta\n->")
    
    rota = ro.generate_rota()
    
    chamadinha = f'ghz --insecure --async \
  --proto ../protos/{ghz_proto}\
  --call {rota} \
  -c 1 -n 1000 --rps 200 \
  -d \'{ghz_final}\' {ip}:{porta} | cat -> ../results/{str(datetime.datetime.now().strftime("%c")).replace(" ", "")}.json'
  
    os.system(chamadinha)
    print("Finalizado")
        
generate_ghz()