import os
def tratamento_package():
    proto = open('../protos/saldo.proto', 'r')

    for msg in proto:
        if 'package' in msg:
            msg_tratada = msg.split()
            package = msg_tratada[1].replace(";", "")
    proto.close()
    return package


def tratamento_rpc():
    rpcs = []
    proto = open('../protos/saldo.proto', 'r')

    for msg in proto:
        if 'rpc' in msg:
            msg_tratada = msg.split()
            if msg_tratada[0] == 'rpc':
                rpc = msg_tratada[1].replace(";", "")
                rpcs.append(rpc)
    proto.close()
    return rpcs

def tratamento_service():
    proto = open('../protos/saldo.proto', 'r')

    for msg in proto:
        if 'service' in msg:
            msg_tratada = msg.split()
            service = msg_tratada[1].replace(";", "")

    proto.close()
    return service

def tratamento_proto():
    ajuda = []
    for diretorio, subpasta, arquivos in os.walk('../protos', topdown=False, onerror=None,followlinks=True):
        for arquivo in arquivos:
            ajuda.append(arquivo)
    return ajuda

